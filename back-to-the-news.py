import requests
from bs4 import BeautifulSoup
from datetime import datetime

url = 'https://news.ycombinator.com/'
response = requests.get(url)
soup = BeautifulSoup(response.text, "html.parser")
stories = soup.findAll("a", {"class": "storylink"})
for index in range(0, len(stories)):
    rank = index + 1
    link = stories[index]['href']
    title = stories[index].contents[0]
    now = datetime.now()
    time = now.strftime("%d/%m/%Y %H:%M:%S")
    #print(f"{rank} {title} ({link}) - {time}")
    print(f"{rank} {title}")
